#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <algorithm>

template <typename T>
class Sequence {
 private:
  T* array;
  size_t size;
  size_t capacity;
  // Methods
  void Resize(size_t new_size);

 public:
  // Constructors
  Sequence();
  Sequence(size_t _size);
  Sequence(const Sequence<T>& other);
  // Operators
  Sequence<T>& operator=(Sequence<T> other);
  Sequence<T>& operator=(Sequence<T>&& other);
  T& operator[](size_t index);
  const T& operator[](size_t index) const;
  // Methods
  int Size() const;
  void Append(T item);
  // Destructor
  ~Sequence();
};

template <typename T>
Sequence<T>::Sequence() : size(0), capacity(1), array(new T[1]) {}

template <typename T>
Sequence<T>::Sequence(size_t _size)
    : size(_size), capacity(_size), array(new T[_size]) {}

template <typename T>
Sequence<T>::Sequence(const Sequence<T>& other)
    : size(other.size), capacity(other.size), array(new T[other.size]) {
  std::copy(other.array, other.array + size, array);
}

template <typename T>
Sequence<T>& Sequence<T>::operator=(Sequence<T> other) {
  if (this == &other) return *this;
  std::swap(size, other.size);
  std::swap(capacity, other.capacity);
  std::swap(array, other.array);
  return *this;
}

template <typename T>
Sequence<T>& Sequence<T>::operator=(Sequence<T>&& other) {
  if (this == &other) return *this;
  std::swap(size, other.size);
  std::swap(capacity, other.capacity);
  std::swap(array, other.array);
  return *this;
}

template <typename T>
T& Sequence<T>::operator[](size_t index) {
  return array[index];
}

template <typename T>
const T& Sequence<T>::operator[](size_t index) const {
  return array[index];
}

template <typename T>
int Sequence<T>::Size() const {
  return size;
}

template <typename T>
void Sequence<T>::Resize(size_t new_size) {
  T* tmp = new T[new_size];
  size_t copy_size = new_size < size ? new_size : size;
  std::copy(array, array + copy_size, tmp);
  if (array) delete[] array;
  std::swap(array, tmp);
  capacity = new_size;
}

template <typename T>
void Sequence<T>::Append(T item) {
  if (size == capacity) Resize(capacity <<= 1);
  array[size++] = item;
}

template <typename T>
Sequence<T>::~Sequence() {
  if (array) delete[] array;
  size = 0;
}

#endif
