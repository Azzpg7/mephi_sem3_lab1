#include <string.h>
#include <time.h>

#include <chrono>
#include <fstream>
#include <iostream>

#include "sequence.h"
#include "sorters.h"

namespace options {

bool output = false;
std::string output_file = "output";
bool shell_sort = false;
bool quick_sort = false;
bool heap_sort = false;
int size = 1e5;
int step = 1e3;

void parse(int argc, char* argv[]) {
  int i = 1;
  while (i < argc) {
    if (strcmp(argv[i], "--shell") == 0)
      shell_sort = true;
    else if (strcmp(argv[i], "--quick") == 0)
      quick_sort = true;
    else if (strcmp(argv[i], "--heap") == 0)
      heap_sort = true;
    else if (strcmp(argv[i], "--output") == 0) {
      output = true;
      output_file = std::string(argv[++i]);
    } else if (strcmp(argv[i], "--size") == 0)
      size = atoi(argv[++i]);
    else if (strcmp(argv[i], "--step") == 0)
      step = atoi(argv[++i]);
    ++i;
  }
}

};  // namespace options

int cmp(const int& l, const int& r) { return l - r; }

int main(int argc, char* argv[]) {
  std::cout << "Parsing arguments...\n";
  options::parse(argc, argv);
  auto sh = ShellSorter<int>();
  auto sh_time = new Sequence<double>();
  auto q = ShellSorter<int>();
  auto q_time = new Sequence<double>();
  auto hs = ShellSorter<int>();
  auto hs_time = new Sequence<double>();
  auto sizes = new Sequence<double>();

  srand(time(0));
  auto seq = new Sequence<int>();
  int cur_size = 0;
  std::cout << "Sorting...\n";
  while (cur_size < options::size) {
    for (int i = 0; i < options::step; ++i) seq->Append(rand());
    cur_size += options::step;
    sizes->Append(cur_size);
    if (options::shell_sort) {
      auto start = std::chrono::high_resolution_clock::now();
      sh.Sort(seq, cmp);
      auto end = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double, std::milli> time = end - start;
      sh_time->Append(time.count());
    }
    if (options::quick_sort) {
      auto start = std::chrono::high_resolution_clock::now();
      q.Sort(seq, cmp);
      auto end = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double, std::milli> time = end - start;
      q_time->Append(time.count());
    }
    if (options::heap_sort) {
      auto start = std::chrono::high_resolution_clock::now();
      hs.Sort(seq, cmp);
      auto end = std::chrono::high_resolution_clock::now();
      std::chrono::duration<double, std::milli> time = end - start;
      hs_time->Append(time.count());
    }
  }

  std::fstream of;
  of.open(options::output_file + ".csv", std::ios::out);
  if (options::shell_sort) {
    of << "shell,";
    for (int i = 0; i < sh_time->Size(); ++i) of << (*sh_time)[i] << ',';
    of << '\n';
  }
  if (options::quick_sort) {
    of << "quick,";
    for (int i = 0; i < q_time->Size(); ++i) of << (*q_time)[i] << ',';
    of << '\n';
  }
  if (options::heap_sort) {
    of << "heap,";
    for (int i = 0; i < hs_time->Size(); ++i) of << (*hs_time)[i] << ',';
    of << '\n';
  }
  of << "size,";
  for (int i = 0; i < sizes->Size(); ++i) of << (*sizes)[i] << ',';
  of << '\n';
  of.close();
  std::string cmd = "python3 ./plot.py " + options::output_file;
  system(cmd.c_str());
}
