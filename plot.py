import sys
import matplotlib.pyplot as plt


def save_plot(fname: str) -> None:
    d: dict[str, list] = {}
    with open(fname + ".csv", "r") as f:
        for line in f:
            # print(line)
            li = list(line.split(','))
            d[li[0]] = list(map(float, li[1:-1]))

    # for a, b in d.items():
    #     print(a, b)
    x = d['size']
    d.pop('size')

    for srt, time in d.items():
        print(f"plotting {srt} graph")
        plt.plot(x, time, label=srt)

    plt.xlabel('sequence size')
    plt.ylabel('time - ms')
    plt.legend(loc='upper left')
    plt.savefig(fname + ".png")


if __name__ == "__main__":
    fname = sys.argv[1]
    save_plot(fname)
