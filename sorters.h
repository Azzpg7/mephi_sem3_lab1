#ifndef SORTERS_H
#define SORTERS_H

#include <time.h>

#include "sequence.h"

template <typename T>
class ISorter {
 public:
  virtual Sequence<T> *Sort(Sequence<T> *seq,
                            int (*cmp)(const T &left, const T &right)) = 0;
};

template <typename T>
class ShellSorter : public ISorter<T> {
 private:
  size_t gap;

 public:
  ShellSorter() : gap(0) {}
  // ShellSorter(size_t _gap) : gap(_gap) {}

  Sequence<T> *Sort(Sequence<T> *seq,
                    int (*cmp)(const T &left, const T &right)) override {
    auto res = new Sequence<T>(*seq);
    int n = res->Size();
    gap = n / 2;
    for (int offset = gap; offset > 0; offset >>= 1) {
      for (int i = offset; i < n; ++i) {
        T toInsert = (*res)[i];
        int j = i;
        while (j >= offset && cmp((*res)[j - offset], toInsert) > 0) {
          (*res)[j] = (*res)[j - offset];
          j -= offset;
        }
        (*res)[j] = toInsert;
      }
    }
    return res;
  }
};

template <typename T>
class QuickSorter : public ISorter<T> {
 private:
  void QSort(Sequence<T> *seq, size_t l, size_t r,
             int (*cmp)(const T &left, const T &right)) {
    size_t i = l;
    size_t j = r;
    size_t pivot = l + rand() % (r - l + 1);
    while (i <= j) {
      while (cmp((*seq)[i], (*seq)[pivot]) < 0) ++i;
      while (cmp((*seq)[j], (*seq)[pivot]) > 0) --j;
      if (i <= j) std::swap((*seq)[i++], (*seq)[j--]);
    }
    if (j > l) QSort(seq, l, j, cmp);
    if (i < r) QSort(seq, i, r, cmp);
  }

 public:
  QuickSorter() { srand(time(0)); }
  QuickSorter(int seed) { srand(seed); }

  Sequence<T> *Sort(Sequence<T> *seq,
                    int (*cmp)(const T &left, const T &right)) override {
    auto res = new Sequence<T>(*seq);
    QSort(res, 0, res->Size() - 1, cmp);
    return res;
  }
};

template <typename T>
class HeapSorter : public ISorter<T> {
 private:
  void Heapify(Sequence<T> *seq, size_t n, size_t root,
               int (*cmp)(const T &left, const T &right)) {
    size_t biggest = root;
    size_t l = root * 2 + 1;
    size_t r = root * 2 + 2;
    if (l < n && cmp((*seq)[l], (*seq)[biggest]) > 0) biggest = l;
    if (r < n && cmp((*seq)[r], (*seq)[biggest]) > 0) biggest = r;
    if (biggest != root) {
      std::swap((*seq)[root], (*seq)[biggest]);
      Heapify(seq, n, biggest, cmp);
    }
  }

 public:
  Sequence<T> *Sort(Sequence<T> *seq,
                    int (*cmp)(const T &left, const T &right)) override {
    auto res = new Sequence<T>(*seq);
    int n = res->Size();
    for (int i = n / 2 - 1; i >= 0; --i) Heapify(res, n, i, cmp);
    for (int i = n - 1; i > 0; --i) {
      std::swap((*res)[0], (*res)[i]);
      Heapify(res, i, 0, cmp);
    }
    return res;
  }
};

#endif
